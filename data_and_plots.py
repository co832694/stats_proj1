from exercise2 import *
from exercise3 import *

def main():
    """
    """
    print("Exercise 2")
    print("==========")
    pmf_cdf_plot(np.linspace(0,3,4,dtype=int),np.linspace(0,4,5,dtype=int),'blue','red')
    pmf_cdf_plot(np.linspace(0,7,8,dtype=int),np.linspace(0,29,30,dtype=int),'orange','green')
    fair_stipple()
    print("==========")
    print("Exercise 3")
    print("==========")
    exercise3()
    
    return

main()
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sps


def q2_q4(data):
    """
    """   
    rain_days = 0
    rd_cond_bf = 0
    
    belfreez_days = 0
    bf_cond_rd = 0
    
    intersect = 0
    
    length = len(data[:,0])
    
    for i in range(0,length):
        
        if data[i,2] != 0:
            rain_days += 1
            if data[i,3] < 0:
                rd_cond_bf += 1
            
        if data[i,3] < 0:
            belfreez_days += 1
            if data[i,2] != 0:
                bf_cond_rd += 1
                
        if data[i,2] != 0 and data[i,3] < 0:
            intersect += 1
    
    print("No. of rainy days / Total no. of days = ", rain_days, " / ", length, " = ",rain_days/length)
    print("No. of below freezing days / Total no. of days = ", belfreez_days, " / ", length, " = ", belfreez_days/length)    
    print("")
    print("No. of below freezing days given it has rained / Total no. of rainy days = "\
          ,intersect, " / ", rain_days, " = ", intersect/rain_days)
    print("No. of rainy days given it is below freezing / Total no. of below freezing days = "\
          , intersect, " / ", belfreez_days, " = ",intersect/belfreez_days)
    
    return

def data_analysis(dataset):
    """
    """
    print(sps.describe(dataset[:,2]))
    print('')
    print(sps.describe(dataset[:,3]))
    print('')
    
    precip = dataset[:,2]
    
    precip_edit = []
    for i in range(len(precip)):
        if precip[i] < 50:
            precip_edit.append(dataset[i,2])
            
    cols = ['black','r']
    labs=['No Snow','Snow']
    
    plt.figure()
    plt.scatter(dataset[:,2],dataset[:,3],20,'black')
    plt.xlabel('Precip. (mm)')
    plt.ylabel('Tmin (degC)')
    #plt.savefig('plots/initial_scatter.pdf')
    
    plt.figure()
    for j in range(2):
        inds = dataset[:,1]==j
        plt.scatter(dataset[inds,2],dataset[inds,3],20,cols[j],label=labs[j])
        plt.xlabel('Precip. (mm)')
        plt.ylabel('Tmin (degC)')
        plt.tick_params(labelsize=16)  
        plt.legend(loc=1)
    #plt.savefig('plots/scatter_colour.pdf')
     
    plt.figure()
    for k in range(2):
        inds = dataset[:,1]==k
        plt.scatter(dataset[inds,2],dataset[inds,3],20,cols[k],label=labs[k])
        plt.xlabel('Precip. (mm)')
        plt.ylabel('Tmin (degC)')
        plt.xlim(-1,35)
        plt.ylim(-1,14)
        plt.tick_params(labelsize=16)  
        plt.legend(loc=1)
        #plt.savefig('plots/scatterinhance.pdf')
        
    plt.figure()
    box = plt.boxplot(dataset[:,3],patch_artist=True) 
    plt.setp(box['boxes'],linewidth=2,facecolor='White')  
    plt.setp(box['whiskers'], color='Red', linewidth=2)
    plt.setp(box['caps'], color='Red', linewidth=2)
    plt.setp(box['fliers'], color='Red', marker='*', markersize=15)
    plt.setp(box['medians'], color='Red', linewidth=2)
    plt.ylabel('Tmin (degC)')
    plt.tick_params(labelsize=16) 
    #plt.savefig('plots/tempboxplot.pdf')
    
    plt.figure()
    plt.hist(dataset[:,2],bins = 70,color='red')
    plt.xlabel('Precip. (mm)')
    plt.ylabel('Count')
    plt.savefig('plots/hist_precip.pdf')
    
    plt.figure()
    plt.hist(precip_edit,bins = 30,color='red')
    plt.xlabel('Precip. (mm)')
    plt.ylabel('Count')
    #plt.savefig('plots/hist_precip_edited.pdf')
    
    plt.figure()
    plt.hist(dataset[:,3],bins = 10,color='red')
    plt.xlabel('Tmin (degC)')
    plt.ylabel('Count')
    #plt.savefig('plots/hist_temp.pdf')
        
    return

def exercise3():
    """
    """
    
    data = np.genfromtxt("christmas.csv",delimiter=",",skip_header=1)
    
    data_analysis(data)  
    q2_q4(data)
    
    return

#exercise3()
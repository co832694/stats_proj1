import numpy as np
from scipy.special import comb
import matplotlib.pyplot as plt

def binom(x,p,N):
    """
    """
    coeff = comb(N,x)
    p = coeff * p**x * (1 - p)**(N - x)

    return p

def pmf_cdf_bin(om,p,N):
    """
    """
    pmf = np.zeros(len(om))
    cmf = np.zeros(len(om))
    
    for i in om:
        pmf[i] = binom(i,p,N)
        cmf[i] = cmf[i-1] + pmf[i]
        
    return pmf, cmf

def plotfunc(N1,N2,om3,om4,pmf3,pmf4,cmf3,cmf4,Var,var,col):
    """
    """   
    fig1 = plt.figure()
    fig1.subplots_adjust(hspace=0.8, wspace=0.8)
    plt.subplot(2,2,1)
    plt.scatter(om3,pmf3,color=col)
    plt.title("pmf plot when N = "+str(N1))
    plt.ylabel("P("+str(Var)+" = "+str(var)+")")
    plt.xlabel("Sample Space")
    plt.ylim(-0.1,1.2)
    plt.subplot(2,2,2)
    plt.scatter(om3,cmf3,color=col)
    plt.title("cmf plot when N = "+str(N1))
    plt.ylabel("P("+str(Var)+" <= "+str(var)+")")
    plt.xlabel("Sample Space")
    plt.ylim(-0.1,1.2)
    plt.subplot(2,2,3)
    plt.scatter(om4,pmf4,color=col)
    plt.title("pmf plot when N = "+str(N2))
    plt.ylabel("P("+str(Var)+" = "+str(var)+")")
    plt.xlabel("Sample Space")
    plt.ylim(-0.1,1.2)
    plt.subplot(2,2,4)
    plt.scatter(om4,cmf4,color=col)
    plt.title("cmf plot when N = "+str(N2))
    plt.ylabel("P("+str(Var)+" <= "+str(var)+")")
    plt.xlabel("Sample Space")
    plt.ylim(-0.1,1.2)
    #plt.savefig("plots/2-3plots_"+str(col)+".pdf")
    plt.show()
    return

def surv_plot(N1,N2,om3,om4,surv3,surv4,Var,var,col):
    """
    """
    fig2 = plt.figure()
    fig2.subplots_adjust(hspace=0.8, wspace=0.8)
    plt.subplot(2,2,1)
    plt.scatter(om3,surv3,color=col)
    plt.title("surv plot when N = "+str(N1))
    plt.ylabel("P("+str(Var)+" > "+str(var)+")")
    plt.xlabel("Sample Space")
    plt.ylim(-0.1,1.2)
    plt.subplot(2,2,3)
    plt.scatter(om4,surv4,color=col)
    plt.title("surv plot when N = "+str(N2))
    plt.ylabel("P("+str(Var)+" > "+str(var)+")")
    plt.xlabel("Sample Space")
    plt.ylim(-0.1,1.2)
    #plt.savefig("plots/survi.pdf")
    plt.show()
    return

def matching_func(N,pmf_ini):
    """
    """
    
    if (N % 2) == 0:
        Nnew = int(N/2)
        count = [Nnew]
        
        pmfY = np.zeros(Nnew + 1)
        cmfY = np.zeros(Nnew + 1)
        
        pmfY[0] = pmf_ini[Nnew]
        cmfY[0] = pmfY[0]
        
        for i in range(1,Nnew + 1):
            pmfY[i] = 2 * pmf_ini[Nnew + i]
            cmfY[i] = cmfY[i - 1] + pmfY[i]
            count.append(Nnew + i)
    else:
        Nnew = int((N + 1)/2)
        count = [Nnew]
        
        pmfY = np.zeros(Nnew)
        cmfY = np.zeros(Nnew)
        
        pmfY[0] = 2 * pmf_ini[Nnew]
        cmfY[0] = pmfY[0]
        
        for i in range(1,Nnew):
            pmfY[i] = 2 * pmf_ini[Nnew + i]
            cmfY[i] = cmfY[i - 1] + pmfY[i]
            count.append(Nnew + i)
    return pmfY, cmfY, count

def surv(N,cmf):
    """
    """
    surviv = np.zeros(N)
    surviv[0] = 1
    
    for i in range(1,N):
        surviv[i] = 1 - cmf[i]
        
    return surviv

def pmf_cdf_plot(om3,om4,col1,col2):
    """
    """    
    N1 = len(om3) - 1
    N2 = len(om4) - 1
    
    pmf3, cmf3 = pmf_cdf_bin(om3,0.5,N1)
    pmf4, cmf4 = pmf_cdf_bin(om4,0.5,N2)
    
    
    pmf3Y, cmf3Y, surv_om3 = matching_func(N1,pmf3)
    pmf4Y, cmf4Y, surv_om4 = matching_func(N2,pmf4)
    
    cN1 = len(cmf3Y)
    cN2 = len(cmf4Y)
    
    surv3Y = surv(cN1,cmf3Y)
    surv4Y = surv(cN2,cmf4Y)
    
    plotfunc(N1,N2,om3,om4,pmf3,pmf4,cmf3,cmf4,'X','x',col1)
    plotfunc(N1,N2,surv_om3,surv_om4,pmf3Y,pmf4Y,cmf3Y,cmf4Y,'Y','y',col2)
    surv_plot(N1,N2,surv_om3,surv_om4,surv3Y,surv4Y,'Y','y','black')

    return

def fair_stipple():
    xtot = 0
    for i in range(0,25):
        print(29 - i)
        x = binom(29-i,1/2,29)
        print("x = ", x)
        xtot += x
        print("xtot = ", xtot)
        if xtot >= 0.125:
            break
        
    return

#pmf_cdf_plot(np.linspace(0,3,4,dtype=int),np.linspace(0,4,5,dtype=int),'blue','red')
#pmf_cdf_plot(np.linspace(0,7,8,dtype=int),np.linspace(0,29,30,dtype=int),'orange','green')
#fair_stipple()

